module.exports = {
  stripPrefix: 'public/',
  staticFileGlobs: [
    '*.html',
    'manifest.json',
    'public/**/!(*map*)'
  ],
  dontCacheBustUrlsMatching: /\.\w{8}\./,
  swFilePath: 'public/sw.js'
};