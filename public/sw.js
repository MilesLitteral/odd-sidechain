self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open('v1').then(function(cache) {
      return cache.addAll([
        '/',
        '/favicon.ico',
        'https://oddjobsapp.xyz',
        'https://oddjobsapp.xyz/css/coming-sssoon.css',
        'https://oddjobsapp.xyz/index.html',
        'https://oddjobsapp.xyz/pwa.webmanifest',
      ]);
    })
  );
});


self.addEventListener('fetch', function(event) {
  event.respondWith(caches.match(event.request).then(function(response) {
    // caches.match() always resolves
    // but in case of success response will have value
    if (response !== undefined) {
      return response;
    } else {
      return fetch(event.request).then(function (response) {
        // response may be used only once
        // we need to save clone to put one copy in cache
        // and serve second one
        let responseClone = response.clone();
        
        caches.open('v1').then(function (cache) {
          cache.put(event.request, responseClone);
        });
        return response;
      }).catch(function () {
        return caches.match('https://oddjobsapp.xyz/images/AddGear.png');
      });
    }
  }));
});