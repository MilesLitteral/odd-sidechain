// Copyright 2018, Google LLC.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';
require('dotenv').config();

const express = require('express'), redirect = require("express-redirect");
const bodyParser = require('body-parser')
const firebase = require('firebase');
const stripe = require("stripe")("pk_live_BHwxoCevzOJ60lj2lYPKBikf");
const bwipjs = require('bwip-js');
const algorithmia = require("algorithmia");
const client = algorithmia("simCLDYkZzvhGd3C/2QJMDZtKwF1");
const fs = require("fs");
const Exponent = require('expo-server-sdk');
const NodeGeocoder = require('node-geocoder');
const NodeRSA = require('node-rsa');
const Neo = require('@cityofzion/neo-js').Neo;
const RippleAPI = require('ripple-lib').RippleAPI;
const iot = require('@google-cloud/iot');
const { Api, JsonRpc, RpcError } = require('eosjs');
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig');      
const fetch = require('node-fetch');                                    
const { TextEncoder, TextDecoder } = require('util');       
const api = new RippleAPI({
  server: 'wss://s1.ripple.com' // Public rippled server
});


const exec = require('child_process').exec;
//const BodyParser = require("body-parser");
const Speakeasy = require("speakeasy");
var app = express();
const passport = require('passport');
const SnapchatStrategy = require('passport-snapchat').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const WechatStrategy = require('passport-wechat').Strategy;
var mqtt = require('mqtt');
var clientMQTT  = mqtt.connect(process.env.G_CLOUD_MQTT_CHANNEL)

var config = {
  apiKey: process.env.G_CLOUD_SERVER_API_KEY,
  authDomain: process.env.G_CLOUD_AUTH_DOMAIN,
  databaseURL: process.env.G_CLOUD_DATABASE,
  projectId: process.env.G_CLOUD_PROJECT_ID,
  storageBucket: process.env.G_CLOUD_STORAGE_BUCKET,
  messagingSenderId: process.env.G_CLOUD_MESSAGING_SENDER
};

firebase.initializeApp(config);
redirect(app);

//app.use(passport.initialize());
//app.use(passport.session());

/*passport.serializeUser(function(user, cb) {
  cb(null, user);
});

passport.deserializeUser(function(obj, cb) {
  cb(null, obj);
});*/

//MQTT/IOT Functionality
clientMQTT.on('connect', function () {
  clientMQTT.subscribe('presence', function (err) {
    if (!err) {
      clientMQTT.publish('presence' + Math.random(), 'Hello mqtt!')
    }
  })
})
 
clientMQTT.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString())
  clientMQTT.end()
})

/*client.on('connect', success => {
  console.log('connect');
  if (!success) {
    console.log('Client not connected...');
  } else if (!publishChainInProgress) {
    publishAsync(mqttTopic, client, iatTime, 1, numMessages, connectionArgs);
  }
});*/

clientMQTT.on('close', () => {
  console.log('close');
  //shouldBackoff = true;
});

clientMQTT.on('error', err => {
  console.log('error', err);
});

clientMQTT.on('message', (topic, message) => {
  let messageStr = 'Message received: ';
  if (topic === `/devices/${deviceId}/config`) {
    messageStr = 'Config message received: ';
  } else if (topic === `/devices/${deviceId}/commands`) {
    messageStr = 'Command message received: ';
  }

  messageStr += Buffer.from(message, 'base64').toString('ascii');
  console.log(messageStr);
});

clientMQTT.on('packetsend', () => {
  // Note: logging packet send is very verbose
});

// Once all of the messages have been published, the connection to Google Cloud
// IoT will be closed and the process will exit. See the publishAsync method.

//Web Functionality
app.use(express.static('public'));
//app.set('views', __dirname + '/views');
//app.set('view engine', 'ejs');

//the View Engine (EJS/Jade) are critical to ARW
//Currently untested(will it break HTML rendering?)
//app.set('view engine', 'jade');

//app.use(BodyParser.json());
//app.use(BodyParser.urlencoded({ extended: true }));

//Johnny-Five(J5) Functions
/* // LED Pin variable
   // Test file for using the Raspberry Pi and Johnny-Five
const five = require('johnny-five');
const raspi = require('raspi-io');

// Make a new `Board()` instance and use raspi-io
const board = new five.Board({
        io: new raspi(),
});

// Run Board
board.on('ready', function() {

        // LED Pin variable
        const led = new five.Led('P1-7');

        led.on();

        this.repl.inject({
                on: () => {
                        led.on();
                },

                off: () => {
                        led.stop().off();
                },

                strobe: () => {
                        led.stop().off();
                        led.strobe();
                },

                blink: () => {
                        led.stop().off();
                        led.blink(500);
                },
        });

        // When this script is stopped, turn the LED off
        // This is just for convenience
        this.on('exit', function() {
                led.stop().off();
        });

});*/



app.post('/ready', function(req, res) {
    // This registration token comes from the client FCM SDKs(RUBY SIDE). 
    var registrationToken = req.body.registrationToken;

    var message = {
      data: {
        command: "led",
        pin: "P1-7",
        blink: 500,
        time: new Date()
      },
      token: registrationToken
    };

    // Send a message to the device corresponding to the provided
    // registration token.
    admin.messaging().send(message)
      .then((response) => {
        // Response is a message ID string.
        console.log('Successfully sent message:', response);
      })
      .catch((error) => {
        console.log('Error sending message:', error);
      });
});

// Snapchat
app.get('/snap-state-build', function(req, res){
  // ******************** Crypto Library Helper ***************************
var _crypto = require('crypto');

var OAUTH2_STATE_BYTES = 32;
var REGEX_PLUS_SIGN = /\+/g;
var REGEX_FORWARD_SLASH = /\//g;
var REGEX_EQUALS_SIGN = /=/g;

/*
 * This function generates a random amount of bytes using the
 * crypto library
 *
 * @param {int} size - The number of random bytes to generate.
 *
 * @returns {Buffer} The generated number of bytes
 */
var generateRandomBytes = function generateRandomBytes(size) {
  return _crypto.randomBytes(size);
};

/*
 * This function encodes the given byte buffer into a base64 URL
 * safe string.
 *
 * @param {Buffer} bytesToEncode - The bytes to encode
 *
 * @returns {string} The URL safe base64 encoded string
 */
var generateBase64UrlEncodedString = function generateBase64UrlEncodedString(bytesToEncode) {
  return bytesToEncode
    .toString('base64')
    .replace(REGEX_PLUS_SIGN, '-')
    .replace(REGEX_FORWARD_SLASH, '_')
    .replace(REGEX_EQUALS_SIGN, '');
};

/*
 * This function generates the state required for both the
 * OAuth2.0 Authorization and Implicit grant flow
 *
 * @returns {string} The URL safe base64 encoded string
 */
var generateClientState = exports.generateClientState = function generateClientState() {
  return generateBase64UrlEncodedString(
    generateRandomBytes(OAUTH2_STATE_BYTES)
  );
};

  //res.send(200, JSON.stringify(generateClientState))
  res.json({ state: generateBase64UrlEncodedString(
    generateRandomBytes(OAUTH2_STATE_BYTES)
  )})
})

app.get('/wechat-auth-file', function(req, res){
  res.sendFile(__dirname + '/public/ZbmARJxaAv.txt')
})

app.get('/wechat-auth-build', function(req, res){
  var _crypto = require('crypto');

  var OAUTH2_STATE_BYTES = 32;
  var REGEX_PLUS_SIGN = /\+/g;
  var REGEX_FORWARD_SLASH = /\//g;
  var REGEX_EQUALS_SIGN = /=/g;
  
  /*
   * This function generates a random amount of bytes using the
   * crypto library
   *
   * @param {int} size - The number of random bytes to generate.
   *
   * @returns {Buffer} The generated number of bytes
   */
  var generateRandomBytes = function generateRandomBytes(size) {
    return _crypto.randomBytes(size);
  };
  
  /*
   * This function encodes the given byte buffer into a base64 URL
   * safe string.
   *
   * @param {Buffer} bytesToEncode - The bytes to encode
   *
   * @returns {string} The URL safe base64 encoded string
   */
  var generateBase64UrlEncodedString = function generateBase64UrlEncodedString(bytesToEncode) {
    return bytesToEncode
      .toString('base64')
      .replace(REGEX_PLUS_SIGN, '-')
      .replace(REGEX_FORWARD_SLASH, '_')
      .replace(REGEX_EQUALS_SIGN, '');
  };

  //process.env.ODD_JOBS_WECHAT_ID
  var getAuthCodeRedirectURL = function getAuthCodeRedirectURL() {
    var WX_ACCOUNTS_LOGIN_URL =  "https://open.weixin.qq.com/connect/qrconnect?appid=wxe747b5e00dbdaa29&redirect_uri=https://odd-jobss.herokuapp.com#login&response_type=code&scope=snsapi_userinfo&state=" + generateBase64UrlEncodedString(generateRandomBytes(OAUTH2_STATE_BYTES)) + "#wechat_redirect";
    return WX_ACCOUNTS_LOGIN_URL;
  };

  // Build redirect URL
  var getRedirectURL = getAuthCodeRedirectURL();

  // Redirect user to get consent
  res.redirect(getRedirectURL);
});


app.get('/snap-auth-build', function(req, res){
  var _qs = require('qs'); // Will need to 'npm install qs'
  var _crypto = require('crypto');

  var OAUTH2_STATE_BYTES = 32;
  var REGEX_PLUS_SIGN = /\+/g;
  var REGEX_FORWARD_SLASH = /\//g;
  var REGEX_EQUALS_SIGN = /=/g;
  
  /*
   * This function generates a random amount of bytes using the
   * crypto library
   *
   * @param {int} size - The number of random bytes to generate.
   *
   * @returns {Buffer} The generated number of bytes
   */
  var generateRandomBytes = function generateRandomBytes(size) {
    return _crypto.randomBytes(size);
  };
  
  /*
   * This function encodes the given byte buffer into a base64 URL
   * safe string.
   *
   * @param {Buffer} bytesToEncode - The bytes to encode
   *
   * @returns {string} The URL safe base64 encoded string
   */
  var generateBase64UrlEncodedString = function generateBase64UrlEncodedString(bytesToEncode) {
    return bytesToEncode
      .toString('base64')
      .replace(REGEX_PLUS_SIGN, '-')
      .replace(REGEX_FORWARD_SLASH, '_')
      .replace(REGEX_EQUALS_SIGN, '');
  };
  
  /*
   * This function generates the state required for both the
   * OAuth2.0 Authorization and Implicit grant flow
   *
   * @returns {string} The URL safe base64 encoded string
   */
  var generateClientState = exports.generateClientState = function generateClientState() {
    return generateBase64UrlEncodedString(
      generateRandomBytes(OAUTH2_STATE_BYTES)
    );
  };

var getAuthCodeRedirectURL = function getAuthCodeRedirectURL(clientId, redirectUri, scopeList, state) {
  var crypto = require('crypto');
  var SNAP_ACCOUNTS_LOGIN_URL = 'https://accounts.snapchat.com/accounts/oauth2/auth';//https://oddjobsapp.xyz/login/snapchat/callback
  var scope = scopeList.join(' ');
  var hash = crypto.createHash('sha256').update(clientId).digest('hex');

  //process.env.ODD_JOBS_SNAPCHAT_ID
  var loginQS = {
    client_id: process.env.ODD_JOBS_SNAPCHAT_ID,
    client_secret: generateBase64UrlEncodedString(generateRandomBytes(OAUTH2_STATE_BYTES)),
    code_challenge: hash.toString(),
    code_challenge_method: "S256",
    redirect_uri: redirectUri,
    response_type: 'code',
    scope: scope,
    state: state
  };

  var stringifyLoginQS = _qs.stringify(loginQS);
  return SNAP_ACCOUNTS_LOGIN_URL + '?' + stringifyLoginQS;
};

  //process.env.ODD_JOBS_SNAPCHAT_ID
  var clientId = process.env.ODD_JOBS_SNAPCHAT_ID;
  var clientSecret = generateBase64UrlEncodedString(generateRandomBytes(OAUTH2_STATE_BYTES))
  var redirectUri = 'https://oddjobsapp.xyz#login' //https://oddjobsapp.xyz/login/snapchat/callback';
  var scopeList = ['https://auth.snapchat.com/oauth2/api/user.display_name', 'https://auth.snapchat.com/oauth2/api/user.bitmoji.avatar'];

  //https://auth.snapchat.com/oauth2/api/user.external_id: Grants access to a stable user ID specific to your app
  //https://auth.snapchat.com/oauth2/api/user.display_name: Grants access to the user's Snapchat display name
  //https://auth.snapchat.com/oauth2/api/user.bitmoji.avatar

  // Generate query parameters
  var state = generateClientState();

  // Build redirect URL
  var getRedirectURL = getAuthCodeRedirectURL(clientId, redirectUri, scopeList, state);

  // Redirect user to get consent
  res.redirect(getRedirectURL);
});

app.get('/facebook-auth-build', function(req, res){
  var _crypto = require('crypto');

  var OAUTH2_STATE_BYTES = 32;
  var REGEX_PLUS_SIGN = /\+/g;
  var REGEX_FORWARD_SLASH = /\//g;
  var REGEX_EQUALS_SIGN = /=/g;
  
  /*
   * This function generates a random amount of bytes using the
   * crypto library
   *
   * @param {int} size - The number of random bytes to generate.
   *
   * @returns {Buffer} The generated number of bytes
   */
  var generateRandomBytes = function generateRandomBytes(size) {
    return _crypto.randomBytes(size);
  };
  
  /*
   * This function encodes the given byte buffer into a base64 URL
   * safe string.
   *
   * @param {Buffer} bytesToEncode - The bytes to encode
   *
   * @returns {string} The URL safe base64 encoded string
   */
  var generateBase64UrlEncodedString = function generateBase64UrlEncodedString(bytesToEncode) {
    return bytesToEncode
      .toString('base64')
      .replace(REGEX_PLUS_SIGN, '-')
      .replace(REGEX_FORWARD_SLASH, '_')
      .replace(REGEX_EQUALS_SIGN, '');
  };
  
  //process.env.ODD_JOBS_FACEBOOK_ID
  var getAuthCodeRedirectURL = function getAuthCodeRedirectURL() {
    var FB_ACCOUNTS_LOGIN_URL = "https://www.facebook.com/v3.3/dialog/oauth?client_id=" + process.env.ODD_JOBS_FACEBOOK_ID + "&redirect_uri=https://odd-jobss.herokuapp.com#login&state=" + generateBase64UrlEncodedString(generateRandomBytes(OAUTH2_STATE_BYTES));//https://oddjobsapp.xyz/login/snapchat/callback
    return FB_ACCOUNTS_LOGIN_URL;
  };

  // Build redirect URL
  var getRedirectURL = getAuthCodeRedirectURL();

  // Redirect user to get consent
  res.redirect(getRedirectURL);
});

app.get('/login/failed', function(req, res){
  res.sendFile("failed.html")
})

app.get('/login/snapchat', function(req, res){
  var _qs = require('qs'); // Will need to 'npm install qs'
  var _crypto = require('crypto');

  var OAUTH2_STATE_BYTES = 32;
  var REGEX_PLUS_SIGN = /\+/g;
  var REGEX_FORWARD_SLASH = /\//g;
  var REGEX_EQUALS_SIGN = /=/g;
  
  /*
   * This function generates a random amount of bytes using the
   * crypto library
   *
   * @param {int} size - The number of random bytes to generate.
   *
   * @returns {Buffer} The generated number of bytes
   */
  var generateRandomBytes = function generateRandomBytes(size) {
    return _crypto.randomBytes(size);
  };
  
  /*
   * This function encodes the given byte buffer into a base64 URL
   * safe string.
   *
   * @param {Buffer} bytesToEncode - The bytes to encode
   *
   * @returns {string} The URL safe base64 encoded string
   */
  var generateBase64UrlEncodedString = function generateBase64UrlEncodedString(bytesToEncode) {
    return bytesToEncode
      .toString('base64')
      .replace(REGEX_PLUS_SIGN, '-')
      .replace(REGEX_FORWARD_SLASH, '_')
      .replace(REGEX_EQUALS_SIGN, '');
  };
  
  /*
   * This function generates the state required for both the
   * OAuth2.0 Authorization and Implicit grant flow
   *
   * @returns {string} The URL safe base64 encoded string
   */
  var generateClientState = exports.generateClientState = function generateClientState() {
    return generateBase64UrlEncodedString(
      generateRandomBytes(OAUTH2_STATE_BYTES)
    );
  };

  //process.env.ODD_JOBS_SNAPCHAT_ID
  passport.use(new SnapchatStrategy({
      clientID: "d2cc3042-3273-4f28-aebd-da8c9d9ad3a1",
      clientSecret: generateBase64UrlEncodedString(generateRandomBytes(OAUTH2_STATE_BYTES)),
      callbackURL: "https://oddjobsapp.xyz/auth/snapchat/callback"
    },
    function(accessToken, refreshToken, profile, cb) {
      User.findOrCreate({ snapchatId: profile.id }, function (err, user) {
        return cb(err, user);
      });
    }
  ));
  
  passport.authenticate('snapchat', { scope: ['user.display_name', 'user.bitmoji.avatar'] })
});

app.get('/login/wechat', function(req, res){
  //process.env.ODD_JOBS_WECHAT_ID
  //process.env.ODD_JOBS_WECHAT_SECRET
  passport.use(new WechatStrategy({
    clientID: "wx6d304cdd7d45d957",
    clientSecret: "1c4a4457d1cf7177c0c632499a32ed3d",
    callbackURL: "https://odd-jobss.herokuapp.com#login"
  },
    function(accessToken, refreshToken, profile, done) {
      return done(err, profile);
    })
  );

  passport.authenticate('wechat'), {scope: ['snsapi_login', 'snsapi_userinfo']};
});

app.get('/login/facebook', function(req, res){
  passport.use(new FacebookStrategy({
    //process.ev.ODD_JOBS_FACEBOOK_ID
      clientID: "313482675903491",
      clientSecret: "41bd23dd42ec7d165b2e8335e1bee452",
      callbackURL: "https://odd-jobss.herokuapp.com#login"
    },
    function(accessToken, refreshToken, profile, done) {
      User.findOrCreate({ facebookId: profile.id }, function(err, user) {
        if (err) { return done(err); }
        done(null, user);
      });
    }
  ));

  passport.authenticate('facebook', { scope: 'read_stream' })
});

app.get('/login/snapchat/callback', passport.authenticate('snapchat', { failureRedirect: '/login/snapchat' }), function(req, res) {
    res.redirect('/');
  });


// Creates the endpoint for our webhook (FB Messenger)
app.post('/webhook', (req, res) => {

// Handles messages events
const handleMessage = (sender_psid, received_message) => {
    let response;
    if (received_message.text) {
      //response.json({message: received_message.text})
    }
}
 
// 
const handlePostback = (sender_psid, received_postback) => {
    let response;
 
    // Get the payload for the postback
    let payload = received_postback.payload;
 
    if(payload === 'GET_STARTED'){
      
    }
}

  let body = req.body;
  if (body.object === 'page') {

      // Iterates over each entry - there may be multiple if batched
      body.entry.forEach(function(entry) {

          // Gets the message. entry.messaging is an array, but
          // will only ever contain one message, so we get index 0
          let webhook_event = entry.messaging[0];
          console.log(webhook_event);

          // Get the sender PSID
          let sender_psid = webhook_event.sender.id;
          console.log('Sender PSID: ' + sender_psid);

          // Check if the event is a message or postback and
          // pass the event to the appropriate handler function
          if (webhook_event.message) {
            handleMessage(sender_psid, webhook_event.message);
          } else if (webhook_event.postback) {
            handlePostback(sender_psid, webhook_event.postback);
          }
      });

      // Returns a '200 OK' response to all requests
      res.status(200).send('EVENT_RECEIVED');
  } else {
      // Returns a '404 Not Found' if event is not from a page subscription
      res.sendStatus(404);
  }
});

//Utilities
app.post('/bugReport', function(req, res){
  var report = req.body.report;
  var id = req.body.id;

  const rep = {id: id, report: report, status: "IN PROGRESS"};

  firebase.database().ref("bugReport/" + id).update({
      id: id,
      report: report
  })

  res.json(rep)
})

//2FA
//firebase.database().ref("users/" +  request.body.email.replace(".", "") + "/2FA/").set({
//secret: secret.base32,
//})
app.post('/totp-secret', function(request, response){
  var secret = Speakeasy.generateSecret({ length: 20 });
  response.json({secret: secret.base32});
});

app.post('/totp-generate', function(request, response){
  response.json({
          token: Speakeasy.totp({
          secret: request.body.secret,
          encoding: "base32"
      }).toString(),
      "remaining": (30 - Math.floor((new Date()).getTime() / 1000.0 % 30))
  });
});

app.post('/totp-validate', function(request, response){
  response.json({
          valid: Speakeasy.totp.verify({
          secret: request.body.secret,
          encoding: "base32",
          token: request.body.token,
          window: 0
      })
  });
});

app.post('/totp-ping', (request, response, next) => {
  //write FCM code here to generate device token and message it.
  //the Modal Popup will generate a TOTP(totp-generate + secret) for the user to input
  // This registration token comes from the client FCM SDKs.
    var registrationToken = request.body.token;
    var message = {
        data: {
          secret: secret,
          token:  Speakeasy.totp({
            secret: request.body.secret,
            encoding: "base32"
          }).toString()
        },
        token: registrationToken
      };

      // Send a message to the device corresponding to the provided registration token.
      firebase.messaging().send(message).then((response) => {
        console.log('Successfully sent message:', response);
      }).catch((error) => {
        console.log('Error sending message:', error);
      });
    });

var pushTokens = [];
app.redirect("http://oddjobsapp.xyz/", "https://oddjobsapp.xyz/", 301);

app.get('/createRSA', function(req, res){
  var key = new NodeRSA({b: 512});
  res.json({rsa: key.exportKey("pkcs1").toString()})
})

//Register Device to IoT Core
app.post('/registerDevice/', (req, res) => {
  // Client retrieved in callback
  // getClient(serviceAccountJson, function(client) {...});
  // push channel: mqtt.googleapis.com
  var crypto = require('crypto');
  var hash = crypto.createHash('md5').update(req.query.pwd).digest('hexya');
  const cloudRegion = process.env.ODD_JOBS_IOT_CLOUD_REGION;
  const deviceId = req.body.deviceID.replace(" ", ""); //'my-rsa-device';
  const projectId = process.env.G_CLOUD_PROJECT_ID;
  const registryId = process.env.ODD_JOBS_REGISTRY_ID;//new Date()
  
  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
  const registryName = `${parentName}/registries/${registryId}`;
  const body = {
    id: deviceId,
    credentials: [
        {
        publicKey: {
            format: 'RSA_X509_PEM',
            key: req.body.rsa,
          },
        },
    ],
  };

  const request = {
    parent: registryName,
    resource: body,
  };

  console.log(JSON.stringify(request));

  clientMQTT.projects.locations.registries.devices.create(request, (err, res) => {
  if (err) {
      console.log('Could not create device');
      console.log(err);
  } else {
      console.log('Created device');
      console.log(res.data);
    }
  });
})

//Send Command
app.post("/sendDeviceMessage", function(req,res){ 
  var crypto = require('crypto');
  var hash = crypto.createHash('md5').update(req.query.pwd).digest('hexya');

   const client = new iot.v1.DeviceManagerClient();
   //process.env.ODD_JOBS_IOT_CLOUD_REGION
   const cloudRegion = 'us-central1';
   const deviceId = req.body.deviceID; //'my-rsa-device';
   //process.env.G_CLOUD_PROJECT_ID
   const projectId = "tensile-tenure-211320";
   const registryId = hash;
   const binaryData = Buffer.from(req.body.commandMessage).toString('base64');

const formattedName = client.devicePath(
  projectId,
  cloudRegion,
  registryId,
  deviceId
);

// NOTE: The device must be subscribed to the wildcard subfolder
// or you should specify a subfolder.
const request = {
  name: formattedName,
  binaryData: binaryData,
  subfolder: req.body.subfolder
};

client.sendCommandToDevice(request)
  .then(() => {
    console.log('Sent command');
  })
  .catch(err => {
    console.error(err);
  });
})

//Patch/Edit RSA Device
app.post("/editDevice", (req, res) => {
    var crypto = require('crypto');
    var hash = crypto.createHash('md5').update(req.query.pwd).digest('hexya');
    // Client retrieved in callback
    // getClient(serviceAccountJson, function(client) {...});
     //process.env.ODD_JOBS_IOT_CLOUD_REGION
    const cloudRegion = 'us-central1';
    const deviceId = req.body.deviceID
       //process.env.G_CLOUD_PROJECT_ID
    const projectId = "tensile-tenure-211320";
    const registryId = hash;
    //process.env.G_CLOUD_PROJECT_ID
    const parentName = `projects/tensile-tenure-211320/topics/iotReg`;
    const registryName = `${parentName}/registries/${registryId}`;

    const request = {
    name: `${registryName}/devices/${req.body.deviceId}`,
    updateMask: 'credentials',
    resource: {
      credentials: [
        {
          publicKey: {
            format: 'RSA_X509_PEM',
            key: req.body.rsa,
          },
        },
      ],
    },
};

clientMQTT.projects.locations.registries.devices.patch(request, (err, res) => {
if (err) {
  console.log('Error patching device:', req.body.deviceId);
  console.log(err);
} else {
  console.log('Patched device:', req.body.deviceId);
  console.log(res.data);
}

  firebase.database().ref("map/" + req.body.userEmail + "/devices/" + req.body.deviceID).set({
          unitID: req.body.deviceID,
          IoTID: req.body.iotID,
          unitModel: req.body.model,
      })
  });
})

app.post("/fireSave", (req, res) => {
  firebase.database().ref("map/" + req.body.userEmail + "/devices/" + req.body.deviceName).set({
      unitID: req.body.deviceID,
      IoTID: req.body.iotID,
      unitModel: req.body.model,
  })
})

//Expo Functionality
app.get('expo/registerToken', function(req, res){
  PushTokens.push(req.token.body)
  res.send(token);
})

app.post('/pushNoti', function(req, res){
  var messages = [];
  for(let pT of PushTokens)
  {
    if(!Exponent.isExpoPushToken(pT))
    {
      console.log("invalid token: " + pT);
      continue;
    }

    //TODO: Cloud Messages (Firebase)?
    messages.push({
      to: pT,
      sound: 'default',
      body:"This is a Test",
      data: { withSome: 'data'},
    })
  }

  let chunks = Exponent.chunkPushNotifications(messages);
  let tickets = [];

  (async () => {
    // Send the chunks to the Expo push notification service. There are
    // different strategies you could use. A simple one is to send one chunk at a
    // time, which nicely spreads the load out over time:
    for (let chunk of chunks) {
      try {
        let ticketChunk = await Exponent.sendPushNotificationsAsync(chunk);
        console.log(ticketChunk);
        tickets.push(...ticketChunk);

        // NOTE: If a ticket contains an error code in ticket.details.error, you
        // must handle it appropriately. The error codes are listed in the Expo
        // documentation:
        // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
      } catch (error) {
        console.error(error);
      }
    }
  })();  
  res.send(200);
})


//merchant.com.oddjobs.technologies
//Stripe Functionality
app.get('/chargeCard', function(req, res){
  var status = stripe.charges.create({
      amount: req.query.value,
      currency: "usd",
      description: 'Payment',
      source: req.query.body,
    })

    res.json({status});
})

app.get('/chargeCardIOS', function(req, res){

  stripe.tokens.create({
    card: {
      number: req.body.cardNo,
      exp_month: 12,
      exp_year: 2020,
      cvc: req.body.cvc
    }
  }, function(err, token) {
    // asynchronously called
  });
  var token = {
  
    expMonth: req.body.expMon,
    expYear: req.body.expYear
  }

  var status = stripe.charges.create({
      amount: req.query.value,
      currency: "usd",
      description: 'iOS Payment',
      source: token,
    })

    res.json({status});
})

app.get('/onboard', function(req, res){
  res.sendFile(__dirname + '/public/onboard.html');
})

app.get('/createCustomer', function(req, res){
  stripe.customers.create({
    email: req.body.email,
    source: req.body.token,
  }, function(err, customer) {
    res.send(customer);
  });
})

//Firebase
app.post('/firebaseSet',function(req, res){
  firebase.database().ref('test/' + req.body.jobTitle).set({Complete: true, TestMsg: req.body.message})
})

app.post('/firebaseGet',function(req, res){
  firebase.database().ref('test/' + req.body.jobTitle).once("value").then(function(snapshot){
    res.json(snapshot)
  })
})

//Cryptography
app.post('/generateSalt', function(req, res){
    var crypto = require('crypto');
    var hash = crypto.createHash('sha256').update(req.query.pwd).digest('hex');
    res.json({salt: hash.toString()})
  //};
})


app.post('/generateMD5', function(req, res){
  var crypto = require('crypto');
  var hash = crypto.createHash('md5').update(req.query.pwd).digest('hexya');
  res.json({md5: hash.toString()})
})

app.get('/updateCustomer', function(req, res){
  stripe.customers.update(req.body.customer, {
    default_source: req.query.token
  });
})

app.get('/chargeCustomer', function(req, res){
  stripe.charges.create({
    amount: req.body.value,
    currency: "usd",
    customer: req.body.customer,
    source: req.body.token,
  }, function(err, charge) {
    // asynchronously called
  });
})

//Implement ARCore: Device, WebARonARCore(Web)
//ARROW - Resume Generator(Based on JSONResume)
//TODO Generate lt, translate it to a document on frontend
app.post('/generateARW', function(req, res){
  console.log(req.body);

  var currentUser = {
    userFullName: req.body.userName,
    userInitials: req.body.userInitials,
    addressLineA: req.body.addressLineA,
    addressLineB: req.body.addressLineB,
    phoneNumber:  req.body.phoneNumber,
    emailAddress: req.body.emailAddress,
    userTwitterHandle: req.body.userTwitter,
    userPinterestHandle: req.body.userPinterest,
    userLinkedInHandle: req.body.userLinkedIn,
    userSkillA: req.body.userSkillA,
    userSkillB: req.body.userSkillB, 
    userSkillC: req.body.userSkillC,
    userSkillD: req.body.userSkillD,
    userEducationA: req.body.userEducationA,
    userEducationB: req.body.userEducationB,
    userEducationC: req.body.userEducationC,
    userEducationD: req.body.userEducationD,
    userJobs: [
      {
        name: req.body.userJobs.name, 
        summary: req.body.userJobs.summary, 
        description: req.body.userJobs.description
      },
    ],
  }
  
  ejs.render(__dirname + "/public/ARWResume.ejs", { user: currentUser });
})

app.get('/debugEJSView', function(req, res){

  ejs.render(__dirname + "/public/ARWResume.ejs", {
    user: {
        "userFullName": "Miles J. Litteral",
        "userInitials": "mjl",
        "addressLineA": "1 Arden St",
        "addressLineB": "New York, NY, 10040",
        "phoneNumber":  "(347)-742-2857",
        "emailAddress": "Mandaloe2@gmail.com",
        "userTwitterHandle": "@litteralera",
        "userPinterestHandle": "MilesLitteral",
        "userLinkedInHandle": "Miles J. Litteral",
        "userSkillA": "College Graduate",
        "userSkillB": "Software Engineer", 
        "userSkillC": "Graphic Designer",
        "userSkillD": "IT Professional",
        "userEducationA": "B.S. Technology",
        "userEducationB": "A+ Certified",
        "userEducationC": "High School Diploma",
        "userEducationD": "N/A",
        "userJobs": [
          {
            "name": "Job A", 
            "summary": "Job Sum", 
            "description": "Job Description"
          }
        ]
    }
  });
})

app.get('/debugEJS', function(req, res){
  res.sendFile(__dirname + "/public/ARWResume.ejs");
})

app.get('/oddBallStart', function(res, req){
  var five = require("johnny-five");
  var board = new five.Board();
  board.on("ready", function(){
    var led = new five.Led(13);
    led.blink(500).then(function(success){
      res.send(success);
    });
  });
})

app.get('/geoLoc', function(req, res){
  //process.env.G_CLOUD_SERVER_API_KEY
    var options = {
      provider: 'google',
      httpAdapter: 'https', 
      apiKey: 'AIzaSyAx3usH0OxGDYgBSR0jzMe3H2DwJ3Ia8Rc', 
      formatter: null
    };

    var geocoder = NodeGeocoder(options);
    geocoder.geocode(req.body.Addr, function(err, resp) {
    res.send(resp);
  });
})

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

app.get('/aqr', function(req, res){
  res.sendFile(__dirname + '/public/ar.html');
})

app.get('/arw', function(req, res) {
  res.sendFile(__dirname + '/public/arw.html');
});

app.post('/pushJob', function(req, res) {
  firebase.database().ref("map/" + req.body.jobTitle).set({
    jobName: req.body.name,
    jobDesc: req.body.desc,
    jobCont: req.body.cont,
    aztec:   req.body.az,
    latitude: req.body.lat,
    longitude: req.body.long
  })
  res.send(200);
})

app.get('/pushAztec', function(req, res) {
  firebase.database().ref("aztec/" + req.query.jobTitle).set({
    aztec:   req.query.az,
  })

  res.send(200);
})

app.post('/pushArw', function(req, res) {
  firebase.database().ref("users/" + req.body.email + "/arw").set({
    arw: req.body.arwCode
  })

  res.set('Content-Type', 'text/html');
  res.send(200, new Buffer('<h2>Test String</h2>'));
})

app.post('/refund', function(req, res){
  stripe.refunds.create({
    charge: req.body.chargeID,
    reverse_transfer: true,
  }).then(function(refund) {
    // asynchronously called
  });
})

app.post('/prodCharge', function(req, res){
  stripe.charges.create({
    amount: 1000,
    currency: "usd",
    source: req.body.token,
    transfer_data: {
      amount: 877,
      destination: "{CONNECTED_STRIPE_ACCOUNT_ID}", // send to Odd Jobs
    },
  }).then(function(charge) {
    // asynchronously called
  });
})

app.post('/createToken', function(res, req){
  var token = stripe.card.createToken({
    number: req.body.cardNo,
    cvc: req.body.cvc,
    exp_month: req.body.expMon,
    exp_year: req.body.expYear,
    address_zip: req.body.zip 
  })

  res.send(200, token);
})

app.post('/direct', function(res, req){
  stripe.charges.create({
    amount: 1000,
    currency: "usd",
    source: req.body.token,
    transfer_data: {
      destination: "{CONNECTED_STRIPE_ACCOUNT_ID}", //Send to Account
    },
  }).then(function(charge) {
    // asynchronously called
  });
})


app.post('/login', function(req, res){
  firebase.auth.signInWithEmailAndPassword(req.body.email, req.body.pass).catch(err => {
    res.send(err)
  })

  res.send({response: "SUCCESS"})
})

app.get('/getArw', function(req, res){
  //let arw = {}
  firebase.database().ref("map/").once("value").then(function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
       //markerL.push(neoMarker(childSnapshot.child("latitude").val(), childSnapshot.child("longitude").val(),   childSnapshot.child("jobTitle").val(), childSnapshot.child("jobDesc").val(), childSnapshot.child("value").val(), childSnapshot.child("accepted").val()))
     })
   })

  res.send(200);
})

app.get('/getAvatar', function(req, res){
  firebase.database().ref("map/").once("value").then(function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
       markerL.push(neoMarker(childSnapshot.child("latitude").val(), childSnapshot.child("longitude").val(),   childSnapshot.child("jobTitle").val(), childSnapshot.child("jobDesc").val(), childSnapshot.child("value").val(), childSnapshot.child("accepted").val()))
     })
   })
  res.send(200);
})

app.get('/logout', function(req, res){
  firebase.database().ref("map/").once("value").then(function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
       markerL.push(neoMarker(childSnapshot.child("latitude").val(), childSnapshot.child("longitude").val(),   childSnapshot.child("jobTitle").val(), childSnapshot.child("jobDesc").val(), childSnapshot.child("value").val(), childSnapshot.child("accepted").val()))
     })
   })
   
  res.sendFile(__dirname + '/public/index.html');
})

app.get('/aztec', function(req, res) {
  res.sendFile(__dirname + '/public/aztec.html');
});

app.get('/chat', function(req, res) {
  res.sendFile(__dirname + '/public/chat.html');
});

app.get('/construction', function(req, res) {
  res.sendFile(__dirname + '/public/construction.html');
});

app.post('/save' ,function(req, res){
  firebase.auth().signInWithEmailAndPassword("mandaloe2@gmail.com", "Getter77").then(response => {
  var ref = firebase.storage().ref();

    ref.put(req.body).then(function(snapshot) {
      console.log('Uploaded a blob or file!');
      res.send(snapshot);
    });
  })
})

app.get('/avatarSave', function(req, res){
  firebase.auth().signInWithEmailAndPassword(req.body.username, req.body.password).then(response => {
  var ref = firebase.storage().ref("/images/");
  var file = new File(req.body.Avatar, req.headers.filename, {
    type: "image/png",
  });

  ref.child(req.headers.email + '/images/' + req.headers.filename).put(file);
  })
})

//EXPERIMENTAL: OJAB Endpoints
//WRITE: Takes a file, writes a Bitmap as Response
//WRITESTRING: takes a string, writes a Bitmap as Response
//READ: takes a JAB Bitmap, responds with a JSON Object
app.get('/OJABWrite', function(req, res){
  const execFile = require('child_process').exec;
  console.log("OJABWrite start"); 
  const child = execFile('./jabcodeWriter' +  [' --input ' + req.query.inputData + ' ' + '--output ' + req.query.fileName  + '.png'], (error, stdout, stderr) => {
  if (error) {
    throw error;
  }

    var src = 'data:image/png;base64,' + stdout.toString('base64');
    res.setHeader('base', '' + src);
    console.log(stdout);
    console.log(stderr);
    res.sendFile(__dirname + "/JAB.png")
  });
})

app.get('/OJABRead', function(req, res){
  const execFile = require('child_process').exec;
  console.log("OJABRead start"); 
  const child = execFile('./jabcodeReader' +  [' --input ' + req.body + ' ' + '--output ' + req.query.fileName  + '.txt'], (error, stdout, stderr) => {
  if (error) {
    throw error;
  }

  var src = 'data:image/png;base64,' + stdout.toString('base64');
  res.setHeader('base', '' + src);
  console.log(stdout);
  console.log(stderr);
  res.sendFile(__dirname + req.query.fileName  + ".txt")
  })
})


//Website Functionality
app.get('/gb', function(req, res) {
  res.sendFile(__dirname + '/public/gb.html');
});

app.get('/gb/arw', function(req, res) {
  res.sendFile(__dirname + '/public/arwGB.html');
});

app.get('/gb/aztec', function(req, res) {
  res.sendFile(__dirname + '/public/aztecGB.html');
});

app.get('/gb/chat', function(req, res) {
  res.sendFile(__dirname + '/public/chatGB.html');
});

app.get('/gb/construction', function(req, res) {
  res.sendFile(__dirname + '/public/constructionGB.html');
});

app.get('/de', function(req, res) {
  res.sendFile(__dirname + '/public/de.html');
});

app.get('/de/arw', function(req, res) {
  res.sendFile(__dirname + '/public/arwDE.html');
});

app.get('/de/aztec', function(req, res) {
  res.sendFile(__dirname + '/public/aztecDE.html');
});

app.get('/de/chat', function(req, res) {
  res.sendFile(__dirname + '/public/chatDE.html');
});

app.get('/de/construction', function(req, res) {
  res.sendFile(__dirname + '/public/constructionDE.html');
});


app.get('/fr', function(req, res) {
  res.sendFile(__dirname + '/public/fr.html');
});

app.get('/fr/arw', function(req, res) {
  res.sendFile(__dirname + '/public/arwFR.html');
});

app.get('/fr/aztec', function(req, res) {
  res.sendFile(__dirname + '/public/aztecFR.html');
});

app.get('/fr/chat', function(req, res) {
  res.sendFile(__dirname + '/public/chatFR.html');
});

app.get('/fr/construction', function(req, res) {
  res.sendFile(__dirname + '/public/html/constructionFR.html');
});

app.get('/cn', function(req, res) {
  res.sendFile(__dirname + '/public/cn.html');
});

app.get('/cn/arw', function(req, res) {
  res.sendFile(__dirname + '/public/arwCN.html');
});

app.get('/cn/aztec', function(req, res) {
  res.sendFile(__dirname + '/public/aztecCN.html');
});

app.get('/cn/chat', function(req, res) {
  res.sendFile(__dirname + '/public/chatCN.html');
});

app.get('/cn/construction', function(req, res) {
  res.sendFile(__dirname + '/public/constructionCN.html');
});

app.get('/it', function(req, res) {
  res.sendFile(__dirname + '/public/it.html');
});

app.get('/it/arw', function(req, res) {
  res.sendFile(__dirname + '/public/arwIT.html');
});

app.get('/it/aztec', function(req, res) {
  res.sendFile(__dirname + '/public/aztecIT.html');
});

app.get('/it/chat', function(req, res) {
  res.sendFile(__dirname + '/public/chatIT.html');
});

app.get('/it/construction', function(req, res) {
  res.sendFile(__dirname + '/public/constructionIT.html');
});

app.get('/es', function(req, res) {
  res.sendFile(__dirname + '/public/es.html');
});

app.get('/es/arw', function(req, res) {
  res.sendFile(__dirname + '/public/arwES.html');
});

app.get('/es/aztec', function(req, res) {
  res.sendFile(__dirname + '/public/aztecES.html');
});

app.get('/es/chat', function(req, res) {
  res.sendFile(__dirname + '/public/chatES.html');
});

app.get('/es/construction', function(req, res) {
  res.sendFile(__dirname + '/public/constructionES.html');
});

app.get('/br', function(req, res) {
  res.sendFile(__dirname + '/public/br.html');
});

app.get('/br/arw', function(req, res) {
  res.sendFile(__dirname + '/public/arwBR.html');
});

app.get('/br/aztec', function(req, res) {
  res.sendFile(__dirname + '/public/aztecBR.html');
});

app.get('/br/chat', function(req, res) {
  res.sendFile(__dirname + '/public/chatBR.html');
});

app.get('/br/construction', function(req, res) {
  res.sendFile(__dirname + '/public/constructionBR.html');
});

app.get('/jp', function(req, res) {
  res.sendFile(__dirname + '/public/jp.html');
});

app.get('/jp/arw', function(req, res) {
  res.sendFile(__dirname + '/public/arwJP.html');
});

app.get('/jp/aztec', function(req, res) {
  res.sendFile(__dirname + '/public/aztecJP.html');
});

app.get('/jp/chat', function(req, res) {
  res.sendFile(__dirname + '/public/chatJP.html');
});

app.get('/jp/construction', function(req, res) {
  res.sendFile(__dirname + '/public/constructionJP.html');
});

app.get('/jp/login', function(req, res) {
  firebase.auth().signInWithEmailAndPassword(req.query.email, req.query.password).then(response =>{
    res.send(response);
  }).catch(err =>{
    res.send(err);
  })
});

app.get('/hello', function(req, res){
  res.status(200);
})

//JSON: {customer: <stripe_customer_id>, email: <firebase auth email>, jobTitle: <string>, value: <USD Cash Value>}
//Odd-Ball Functionality
app.post("/nfc", function(req, res) {
    stripe.charges.create({
        currency: "usd",
        customer: req.body.customer,
        source: req.body.token,
        description: 'Payment',
        statement_descriptor: 'Odd Jobs Technologies LLC',
        destination: {
          // Send $80 to the seller after collecting a 20% platform fee.
          amount: req.body.value,
          // The destination of this charge is the seller's Stripe account.
          account: "acct_1DDMG6EHd2XfT9o1"
        },
        capture: true
    }, function(err, charge) {
        firebase.database().ref('map/' + req.body.jobTitle).set({Complete: true, Terminus: "NFC"})
        res.send(result);
  }).catch((err) => {
        res.send(err)
    })
  })

    //BONES Functionality, Syntax: https://tensile-tenure-211320.appspot.com/bones?code=azteccode&text=ddsssd&scale=3&height=12
    app.get('/bones', function(req, res){
        bwipjs.toBuffer({
            bcid:        req.query.code,       // Barcode type
            text:        req.query.text,    // Text to encode
            scale:       req.query.scale,               // 3x scaling factor
            height:      req.query.height,              // Bar height, in millimeters
            includetext: true,            // Show human-readable text
            textxalign:  'center',        // Always good to set this
        }, function (err, png) {
            if (err) {
                console.log(err)
            } else {
                //In Client, use this to save buffer as PNG
                var src = 'data:image/png;base64,' + png.toString('base64');
                res.setHeader('base', '' + src);
                res.send({base: src});
              }
        });
    })

  //Algorithmia Functionality
  app.get('/createDS', function(req, res){
  var nlp_directory = client.dir("data://OddJobsTechnologies/nlp_directory")
  // Create your data collection if it does not exist
  nlp_directory.exists(function(exists) {
    if (exists == false) {
        nlp_directory.create(function(response) {
            if (response.error) {
                return console.log("Failed to create dir: " + response.error.message);
            }
            console.log("Created directory: " + nlp_directory.data_path);
        });
    } else {
        console.log("Your directory already exists.")
    }
  });
})

  app.post('/uploadData', function(req, res){
    //change string to req.params.datalink, datalink is a string written as follows minus arrows "data://OddJobsTechnologies/nlp_directory/<NameOfData.txt>".
    var text_file = req.params.datalink

    client.file(text_file).exists(function(exists) {
        // Check if file exists, if it doesn't create it
        if (exists == false) {
            nlp_directory.putFile(local_file, function(response) {
                if (response.error) {
                    return console.log("Failed to upload file: " + response.error.message);
                }
                console.log("File uploaded.");
            });
        } else {
            console.log("You're file already exists.")
        }
    });
  })

  app.get('/downloadData', function(req, res){
    //change to req.params.datalink, datalink is a string written as follows minus arrows "data://OddJobsTechnologies/nlp_directory/<NameOfData.txt>".
    var text_file = req.params.datalink
    client.file(text_file).exists(function(exists) {
      if (exists == true) {
          // Download contents of file as a string
          client.file(text_file).get(function(err, data) {
              if (err) {
                  console.log("Failed to download file.");
                  console.log(err);
              } else {
                  console.log("Successfully downloaded data.")
              }
              var input = data;
          });
      }
  });
})

app.get('/fireAllen', function(req, res){
client.file(req.params.datalink).exists(function(exists) {
  if (exists == true) {
      // Download contents of file as a string
      client.file(req.params.datalink).get(function(err, data) {
          if (err) {
              console.log("Failed to download file.");
              console.log(err);
          } else {
              console.log("Successfully downloaded data.")
          }
          var input = data;

          // Call an algorithm with text input by passing a string into the pipe method. The returned promise will be called with the response with the Algorithm completes (or when an error occurs). If the algorithm output is text, then the get() method on the response will return a string.

          client.algo("algo://nlp/Summarizer/0.1.3")
              .pipe(input)
              .then(function(response) {
                  console.log(response.get());
                  res.send(response);
              });
          });
        }
      });
    })

app.get("/nudeDetect", function(req, res){
  var Algorithmia = require("algorithmia");
  //must be a url link, TODO: Implement firebase.storage in app and use
  //it's respective URI lnk
  var input = req.body.image;
  //debug
  //var input = "http://www.isitnude.com.s3-website-us-east-1.amazonaws.com/assets/images/sample/young-man-by-the-sea.jpg";
  //process.env.ODD_JOBS_ALGORITHMIA_API_KEY
  Algorithmia.client("simCLDYkZzvhGd3C/2QJMDZtKwF1")
  .algo("sfw/NudityDetectioni2v/0.2.12")
  .pipe(input)
  .then(function(response) {
        res.send(response.get());
    });
});
  

app.get("/socialSentiment", function(req, res){
var Algorithmia = require("algorithmia");

var input = {
  "sentence": req.body.input
};//process.env.ODD_JOBS_ALGORITHMIA_API_KEY
    Algorithmia.client("simCLDYkZzvhGd3C/2QJMDZtKwF1")
    .algo("nlp/SocialSentimentAnalysis/0.1.4")
    .pipe(input)
    .then(function(response) {
        res.send(response);
    });
})

//Particle Functionality
app.get('/particleToken', function(req, res){
  particle.login({username: req.body.email, password: req.body.pass}).then(
    function(data){
      console.log('API call completed on promise resolve: ', data.body.access_token);
    },
    function(err) {
      console.log('API call completed on promise fail: ', err);
    }
  );
})

app.get('/listDevices', function(req, res){
  var token; // from result of particle.login

  particle.login({username: req.body.email/*'oddjobstechnology@gmail.com'*/, password: req.body.pass/*Getter77*/}).then(
    function(data){
      console.log('API call completed on promise resolve: ', data.body.access_token);
      token = data;
    })

  var devicesPr = particle.listDevices({ auth: token });
  devicesPr.then(
    function(devices){
      console.log('Devices: ', devices);
    },
    function(err) {
      console.log('List devices call failed: ', err);
    }
  );
})


  //remember to add  ML Kit in RN
  //Really it should be decided which is deployed where, seeing as MLKit can work
  //Client side and Algorithmia can take on huge beasts

//NEO
app.get('/getNeoBlock', function(req, res){
  // Create a neo instances to interface with RPC methods
  const testnetNeo = new Neo({ network: 'testnet' })

  // Wait for mesh to be ready before attempt to fetch block information
  testnetNeo.mesh.on('ready', () => {
    testnetNeo.api.getBlockCount()
      .then((res) => {
        console.log('Testnet getBlockCount:', res)
      })
  })

  // To connect to the mainnet:
  const mainnetNeo = new Neo({ network: 'mainnet' })

  const options = {
    network: 'testnet',
    storageType: 'mongodb',
    storageOptions: {
      connectionString: 'mongodb://localhost/neo_testnet',
    },
  }
  
  // Create a neo instance
  const neo = new Neo(options)
  
  // Get block height
  neo.storage.on('ready', () => {
    neo.storage.getHighestBlockHeight()
      .then((res) => {
        console.log('Block height:', res)
      })
  })
})

app.post('/retrieveAddress', function(req, res){
    const neo = new Neo(options)

    var options = {
        baseUrl: 'http://www.antchain.org/api/v1/',
        transform: neo.transforms.antchain
    };

    //test addr: 'AQVh2pG732YvtNaxEGkQUei3YA4cvo7d2i'
    neo.antChain(options).getAddressValue(req.body.NEOAddr).then(function (addressValue) {
        console.log(addressValue.antShare.value);
        console.log(addressValue.antCoin.value);
    });
})

app.post('saveEOS/', function(req, res){
  const defaultPrivateKey = req.body.defaultPrivateKey; //"5JtUScZK2XEp3g9gh7F8bwtPTRAkASmNrrftmx4AxDKD5K4zDnr" 
  const signatureProvider = new JsSignatureProvider([defaultPrivateKey]);

  const rpc = new JsonRpc('https://oddjobsapp.xyz:8888/', { fetch });
  const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

  try {
  (async () => {
  const result = await api.transact({
        actions: [{
        account: 'eosio.token',
        name: 'transfer',
        authorization: [{
            actor: req.body.actorA,
            permission: 'active',
        }],
        data: {
            from: req.body.actorA,
            to: req.body.actorB,
            quantity: req.body.value,
            memo: req.body.memo,
        },
        }]
    }, {
        blocksBehind: 3,
        expireSeconds: 30,
    });
  })()

    res.json(result);
  } catch (e) {
    console.log('\nCaught exception: ' + e);

    if (e instanceof RpcError){
        console.log(JSON.stringify(e.json, null, 2));
    }
  }
})

//Ripple/XRP/Blockchain Functionality
app.post("/WalletCheck", function (req, res) {
api.connect().then(() => {
  /* begin custom code ------------------------------------ */
  const myAddress = req.body.walletAddress;

  console.log('getting account info for', myAddress);
  return api.getAccountInfo(myAddress);

}).then(info => {
  console.log(info);
  console.log('getAccountInfo done');

  /* end custom code -------------------------------------- */
}).then(() => {
  return api.disconnect();
}).then(() => {
  console.log('done and disconnected.');
  res.status(200);
}).catch(console.error);
});

app.post("/ledgerAccept", function(req, res) {
  const request = {command: 'ledger_accept'};
  res.send(api.connection.request(request));
})

app.post("/pay", function(req, res) {
  const paymentSpecification = {
    source: {
      address: req.body.from,
      maxAmount: {
        value: req.body.amount,
        currency: 'XRP'
      }
    },
    destination: {
      address: req.body.to,
      amount: {
        value: req.body.amount,
        currency: 'XRP'
      }
    }
  };

  if (counterparty !== undefined) {
    paymentSpecification.source.maxAmount.counterparty = req.body.counterparty;
    paymentSpecification.destination.amount.counterparty = req.body.counterparty;
  }

  let id = null;
  return api.preparePayment(req.body.from, paymentSpecification, {})
    .then(data => api.sign(data.txJSON, req.body.secret))
    .then(signed => {
      id = signed.id;
      return api.submit(signed.signedTransaction);
    })
    .then(() => ledgerAccept(api))
    .then(() => res.send(id))
})

if (module === require.main) {
  // [START server]
  // Start the server
  const server = app.listen(process.env.PORT || 8080, () => {
    const port = server.address().port;
    console.log(`App listening on port ${port}`);
  });
  // [END server]
}

module.exports = app;